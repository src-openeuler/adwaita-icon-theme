Name:           adwaita-icon-theme
Version:        47.0
Release:        1
Summary:        Adwaita icon theme
License:        LGPLv3+ or CC-BY-SA-3.0
URL:            https://gitlab.gnome.org/GNOME/adwaita-icon-theme
Source0:        https://download.gnome.org/sources/adwaita-icon-theme/47/%{name}-%{version}.tar.xz

BuildArch:      noarch

BuildRequires:  meson
BuildRequires:  /usr/bin/gtk-update-icon-cache

Requires:       adwaita-cursor-theme = %{version}-%{release}

%description
This package contains the Adwaita icon theme used by the GNOME desktop.

%package -n     adwaita-cursor-theme
Summary:        Adwaita cursor theme

%description -n adwaita-cursor-theme
The adwaita-cursor-theme package contains a modern set of cursors originally
designed for the GNOME desktop.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name} = %{version}-%{release}

%description    devel
The %{name}-devel package contains the pkgconfig file for
developing applications that use %{name}.

%prep
%autosetup -n %{name}-%{version} -p1

%build
%meson
%meson_build

%install
%meson_install

touch $RPM_BUILD_ROOT%{_datadir}/icons/Adwaita/.icon-theme.cache

%transfiletriggerin -- %{_datadir}/icons/Adwaita
gtk-update-icon-cache --force %{_datadir}/icons/Adwaita &>/dev/null || :
%transfiletriggerpostun -- %{_datadir}/icons/Adwaita
gtk-update-icon-cache --force %{_datadir}/icons/Adwaita &>/dev/null || :

%files
%defattr(-,root,root)
%license COPYING*
%exclude %{_datadir}/icons/Adwaita/cursors/
%{_datadir}/icons/Adwaita/*/
%{_datadir}/icons/Adwaita/index.theme
%ghost %{_datadir}/icons/Adwaita/.icon-theme.cache

%files -n adwaita-cursor-theme
%defattr(-,root,root)
%license COPYING*
%dir %{_datadir}/icons/Adwaita/
%{_datadir}/icons/Adwaita/cursors/

%files devel
%defattr(-,root,root)
%{_datadir}/pkgconfig/adwaita-icon-theme.pc

%changelog
* Thu Feb 27 2025 zhangpan <zhangpan103@h-partners.com> - 47.0-1
- update to 47.0

* Wed Mar 13 2024 panchenbo <panchenbo@kylinsec.com.cn> - 45.0-2
- fix build error : add missing BuildRequires:/usr/bin/gtk-update-icon-cache

* Mon Jan 8 2024 zhangpan <zhangpan103@h-partners.com> - 45.0-1
- update to 45.0

* Mon Jul 24 2023 zhangpan <zhangpan103@h-partners.com> - 44.0-1
- update to 44.0

* Mon Jan 02 2023 lin zhang <lin.zhang@turbolinux.com.cn> - 43-1
- Update to 43

* Mon Mar 28 2022 lin zhang <lin.zhang@turbolinux.com.cn> - 42.0-1
- Update to 42.0

* Mon Dec 27 2021 wangkerong <wangkerong@huawei.com> - 41.0-1
- update to 41.0
- splite adwaita-cursor-theme subpackage

* Thu Jan 28 2021 yanglu <yanglu60@huawei.com> - 3.38.0-1
- Version update

* Wed Dec 16 2020 hanhui <hanhui15@huawei.com> - 3.37.2-2
- modify url

* Mon Jul 20 2020 wangye <wangye70@huawei.com> - 3.37.2-1
- Version update

* Mon Sep 02 2019 openEuler Buildteam <buildteam@openeuler.org> - 3.32.0-1
- Package init
